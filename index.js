const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data strucure.
// Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin123:admin123@course-booking.e4eg3.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

// Set notofication for connection success or failure
// Connection to the database
// Allows us to handle errors when the initial connection is established
let db = mongoose.connection;
// if a connection error occured, output in our console.
db.on("error", console.error.bind(console, "Connection error"));

// if the connection is successful, output in our console.
db.once("open", () => {console.log("We're connected to the cloud database")});


// Schemas determin the structure of the documents to be written in the database.
// This acts as a blueprints to our data.
const taskSchema = new mongoose.Schema({
	username: String,
	status: {
		type: String,
		default: "pending"
	}
});



// Model uses schemas and are used to create/instantiate objects that correspons to the schema
// Models must be in a singular form and capitalized.
// The first parameter of the mongoose model method indicates the collection in where will be stored in MongoDB collection
// The second parameter is used ti specify the schema/blueprint of the documents
// Using mongoose, the package was progremmed wel lenough that it automatically converts the singular form of the model name into plural form when creating a collection.
const Task = mongoose.model("Task", taskSchema);








// Setup for allowing the server to handle data from requests
// Allows out app to read json data
app.use(express.json());

// Allows our data to read data from forms
app.use(express.urlencoded({extended:true}));


// Creating a new Task
/*
	1. Add a functionality to check if there are diplicate tasks
	 - If the task is already exists in the data base, we return an erorr
	 - If the task tasks doesnt exist, we add it in our database.
	2. The task data will be comming from the request's body
	3. Create a Task object with a "name" field/property

*/

app.post("/tasks", (req,res) => {


	//Check if there are duplicate tasks
	//If there are no matches, the value of result is null
	Task.findOne({name: req.body.name}, (err, result) => {


		//if a document was found and the document's name matches the information sent via the client/Postman
		if (result != null && result.name == req.body.name){
			//Return a message to the client/postman
			return res.send("Duplicate task found");
		}
		//if no document was found
		else {
			//Create a new Task and save it to the database
			let newTask = new Task ({
				name: req.body.name
			});

			//save method will store the information to the
			newTask.save((saveErr, savedTask) =>{

				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				//no error found while creating the
				else {
					//returns a status code of 201 and sends a message "New Task created."
					return res.status(201).send("New task detected")
				}
			})
		}
	})

})

// GET request to retrieve all documents/

/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success message back to the client.
*/

app.get("/tasks", (req, res) => {

	Task.find({}, (err, result) => {

		if(err) {
			console.log(err);
		} else {

			return res.status(200).json({
				data: result
			})
		}
	})


})

// Activity Start




// For Activity
const taskSchema2 = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

// for activity model
const User = mongoose.model("User", taskSchema2);



app.post('/signup', (req, res) => {
	User.findOne({}, (err, result) => {
		if (result != null && result.username === req.body.username){
			console.log(result)
			return res.send("Username Already Taken.");
		}
		else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user registered.")
				}
			})
		}
	})
})

// Activity End




app.get(`/hello`, (req,res)=>{
	res.send("Hello World");
});

app.listen(port, ()=> {console.log(`Server is running at port ${port}`)});